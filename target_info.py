
## Place holder to store asic configuration parameters

from xmlparser import XmlParser
xp = XmlParser()


asic_type_dict={'VMM':0,'TDS':1,'ROC':2,'NONE':3}

class target(object):
	def __init__(self,chip):
		self.guest_ip = "192.168.0.1"
		self.guest_port = 6008
		self.board_type = 0
		self.board_id = 3 #e-link number
		if (chip=="vmm"):
			self.asic_type = asic_type_dict['VMM']
			self.asic_id = 7
			self.cmd_content=xp.vmm_hexgen("vmm.xml")
		elif (chip=="tds"):
			self.asic_type = asic_type_dict['TDS']
			self.asic_id = 0
			self.cmd_content=xp.tds_hexgen("tds.xml")
		else: 
			self.asic_type = asic_type_dict['NONE']
			self.asic_id = 0
			self.cmd_content=''
			
