import xml.etree.cElementTree as ET
from  param2bin import param2bin
p2b=param2bin()

class XmlParser():
	def __init__(self):
		None

	def vmm_hexgen(self,vmm_xml):

		vmmxml=ET.ElementTree(file=vmm_xml)
		vmmRegB=""
		for i,child in enumerate(vmmxml.getroot()):
			for j,elem in enumerate(child.iter()):
				if (j!=0):
					vmmRegB+=str(p2b.parVMM(elem.tag,elem.text))
		LSB_vmmRegB=vmmRegB[::-1]
		LSB_vmmRegH=((hex(int(LSB_vmmRegB,2))[2:]).replace('L','')).zfill(432)
		#print LSB_vmmRegH, len(LSB_vmmRegH)
		return LSB_vmmRegH
		
	def tds_hexgen(self,tds_xml):

		tdsxml=ET.ElementTree(file=tds_xml)
		reglist=[]
		reg_size=0
		bitStream=""

		for i,child in enumerate(tdsxml.getroot()):
			#print i,child.tag,child.attrib
			reg_data=""
			reg_size=int(child.attrib['size'])*2 # 1 byte = 2 hex bits
			for j,elem in enumerate(child.iter()): 
				if (j!=0): #exclude child itself 
					#print elem.tag,elem.text
					reg_data+=str(p2b.parTDS(elem.tag,elem.text))
				else: 
					pass
		
			reghex=(hex(int(reg_data,2))[2:]).replace('L','')
			reglist.append(reghex.zfill(reg_size))
			bitStream+=str(reghex.zfill(reg_size))

		#print "BitStream:",bitStream,len(bitStream)
		return bitStream



if __name__ == "__main__":
	xp=XmlParser()
	xp.vmm_hexgen('vmm.xml')

