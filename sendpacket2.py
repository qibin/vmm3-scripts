#!/usr/bin/env python

#import socket
import binascii
from time import sleep
import serial
from target_info import *

##L's comment:
#NOTE: This script is just a transitional scheme. take no consideration of delay and data collect efficiency. 
# We just send the command/config the VMM, all the data is collect via oscillscope.
# If yoou want to use this to imply full-featured read/write, please use Ethernet or official UCOM library from USTC.

cmd_dict={"InitSCA":0x0100,
	  "CfgASIC":0x0200,
	  "RstASIC":0x0300,
    	  "DoNothing":0xFFFF}

COM='/dev/ttyUSB0'
baudrate=921600
##L:maybe class varible is better than global one?

class SendPacket():
	def __init__(self):
		self.ser = serial.Serial(COM, baudrate, timeout=0.5)  ##L:opend immediately if port is specified
    		if self.ser.isOpen() :
        		print("COM::open:   "+COM+"   success")
		else :
			print("COM::open:   "+COM+"   failed")
			##L:may need a error handle here. MARK.
		self.general_packet_header= binascii.a2b_hex('decafbad') 

	def send(self,chip,cmd_string,target):
		# First Cfg Word [30:29]= board type [28:26]=board id [25:24]=aisc type [23:16]=VMMID Bit Mask [15:0] CMD
		idWord_bin =    (bin(target(chip).board_type)[2:].zfill(3))+ \
				(bin(target(chip).board_id)[2:].zfill(3))+ \
				(bin(target(chip).asic_type)[2:].zfill(2))+ \
				(bin(target(chip).asic_id)[2:].zfill(8))
		idWord_hex = hex(int(idWord_bin,2))[2:].zfill(4)
		cmd_hex = hex(cmd_dict[cmd_string])[2:].zfill(4)
		command_word_32b = binascii.a2b_hex(idWord_hex+cmd_hex)
		payload =binascii.a2b_hex(target(chip).cmd_content) ##L:bytes in str-type varible. 
		##L:Don't know why not use build-in bytes type. 
		##L:And it is very interesting that part of payload is like 0x001001... but not 0b001001..
		message = self.general_packet_header + command_word_32b + payload  
		##L:and pay attention that: a2b_hex:str->(ascii)->bytes->(hex,without '0x')->str. bytes is always the bit-stream in bin like 0b01010....
		mes_vec = [message]
		##L:Why do a once loop??
		for i in range(1):
			print "COM::send:   "+binascii.b2a_hex(mes_vec[i])
			self.ser.write(mes_vec[i])
			#self.ser.write(binascii.b2a_hex(mes_vec[i])) #ascii method
			sleep(0.2)

	def recvall(self):
		recv_count = self.ser.inWaiting()
		if recv_count != 0:
            		recv_data = self.ser.read(recv_count)
			print "COM::recv:   "+binascii.b2a_hex(recv_data)
			#print "COM::recv_ascii:   "+recv_data #ascii method
		else:
			print "COM::recv:   "+"NULL!!"+"	maybe you need loopback Tx-Rx or just set higher timeout"
        	self.ser.flushInput()
		sleep(0.1)
		
if __name__ == "__main__":
	print "SendPacket::Testing..."
	sp=SendPacket()
	#sp.send('','InitSCA',target)
	sp.send('vmm','CfgASIC',target)
	sp.recvall()
	#sp.send('tds','CfgASIC',target)
	#sp.send('vmm','RstASIC',target)
	#sp.send('tds','RstASIC',target)






