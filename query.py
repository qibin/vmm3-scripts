#!/usr/bin/env python
import socket
import binascii
from time import sleep
from target_info import *

class SendPacket():

	def __init__(self):
		self.host_ip="192.168.0.16"
		self.host_port=6007
		self.dest_ip="192.168.0.1"
		self.dest_port=6008
		self.general_packet_header= binascii.a2b_hex('decafbad') 
		

	def send(self):

		cmd_hex = "00000400"

		cmd = binascii.a2b_hex(cmd_hex)

		print "~~~~~~",cmd
		#reg_string = "000000D0000000FF" #Query Elink Status 
		#reg_string = "000000DC000000FF" #Query TTC Satatus
		reg_string = "000000DE000000FF" #Query EvID Status
	
		payload = binascii.a2b_hex(reg_string)	
	
		message = self.general_packet_header + cmd +  payload 
		mes_vec = [message]

		sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		sock.bind((self.host_ip, self.host_port))
		
		for i in range(1):
			print "My IP:     " + self.host_ip
			print "My Port:   " + str(self.host_port)
			print "Dest IP:   " + self.dest_ip
			print "Dest Port: " + str(self.dest_port)
			print "Payload:   " + binascii.b2a_hex(mes_vec[i])
			sock.sendto(mes_vec[i], (self.dest_ip, self.dest_port))
			sleep(0.2)


if __name__ == "__main__":
	sp=SendPacket()
	sp.send()

